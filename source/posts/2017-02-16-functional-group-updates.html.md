---
title: "GitLab's Functional Group Updates"
author: GitLab
author_twitter:
categories:
image_title:
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

## Functional Group updates

Every day from Monday to Thursday, right before our [GitLab team call](https://about.gitlab.com/handbook/#team-call), a different Functional Group gives an [update](https://about.gitlab.com/handbook/people-operations/functional-group-updates/) to our team.
The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our Youtube account and made public, with the exception of the Sales and Finance updates.

Below are the recordings of the updates of the last 3 weeks. We aim to post a blog every week with the latest updates but might combine them.

Discussion team

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8tjVmEe5urI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Product team

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/BysM79IjVzY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

PeopleOps team

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/kwWLUx0n3MQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Infrastructure team

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/I3E24RT1Ajc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

UX team

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/cn3Ho8ez110" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Marketing team

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/csoEkFRmQXY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

CI team

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/9K5foaojQAU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Build team

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8LhyVRshaV4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Support team

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/OsEFTeWDBxA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Interested in helping out one of our many Functional Groups? How about joining one of these teams! Check out our [job openings](https://about.gitlab.com/jobs)
and apply right now!
